import Login from "../pages/Login";
import SignIn from "../pages/SignIn";

const routerRegister = [
  {
    path: "/login",
    Component: Login,
  },
  {
    path: "/signin",
    Component: SignIn,
  },
];

export default routerRegister;
