import Home from "../pages/Home/Home";

const homeRouter = {
  path: "/",
  Component: Home,
};

export default homeRouter;
