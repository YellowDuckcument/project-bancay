import homeRouter from "./routerHome";
import routerRegister from "./routerRegister";
import shopRouter from "./routesShop";

const allRoutes = [homeRouter, shopRouter, ...routerRegister];

export default allRoutes;
