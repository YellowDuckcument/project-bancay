import Shop from "../pages/Shop";

const shopRouter = {
  path: "/shop",
  Component: Shop,
};
export default shopRouter;
