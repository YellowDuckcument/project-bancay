import { Route, Routes } from "react-router-dom";
import "./App.css";
import Footer from "./pages/Footer";
import Header from "./pages/Header";
import allRoutes from "./routes";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <>
      <Header />
      <Routes>
        {allRoutes.map((item) => (
          <Route path={item.path} element={<item.Component />} />
        ))}
      </Routes>
      <Footer />
    </>
  );
}

export default App;
