import React from "react";
import Cover from "../../assets/img/cover_wraper.jpg";
import "./style.scss";

const Home = () => {
  return (
    <>
      <div className="wrapper_top">
        {/* <div className="bg-image">
          <img className="img-fluid h-50 shadow-4" src={Cover} alt="" />
          <div className="d-flex justify-content-center align-items-center h-100">
            <p className="mb-0">Can you see me?</p>
          </div>
        </div> */}
        <div className=" position-relative">
          <img className="shadow-4" src={Cover} alt="" />
          <div className="wrapper_top-block position-absolute">
            <h1 class="mb-3 h2">Jumbotron</h1>

            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Repellendus praesentium labore accusamus sequi, voluptate debitis
              tenetur in deleniti possimus modi voluptatum neque maiores dolorem
              unde? Aut dolorum quod excepturi fugit.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
